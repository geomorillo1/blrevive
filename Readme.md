[![Discord](https://img.shields.io/discord/794628005342216203?label=discord&style=flat-square)](https://discord.gg/wwj9unRvtN)

A user-friendly game patcher for Blacklight: Retribution. The long-term goal is to make the latest Steam release playable without limitations.



> Currently this project is still in alpha and get tested concurrently. If you want a stable version I recommend to check this page weekly or join one of our communities linked below.



# Installation

- Download the latest Blacklight: Retribution Client (version 3.02) from Steam. 

  > If you don't have BL:R in your Steam library you can get a copy here: [Old Wiki Downloads](https://gitlab.com/blrevive/docs/-/wikis/Resources).

- Download the [latest BLRevive]() release.



# Setup

This will enable local and online play for your client.

1. Download the [latest Client (v3.02)](https://gitlab.com/blrevive/docs/-/wikis/Resources)
2. Download the [latest BLRevive]() release.
3. Extract  `BLRevive.zip` and copy the contents of `BLRevive` into ` Binaries/Win32` of the base directory of your BL:R client (eg. `C:\\Program Files(x86)\\Steam\\steamapps\\common\\blacklightretribution\\Binaries\\Win32`)
4. Verify that the `Binaries\Win32` subdirectory contains a `BLRevive.exe`



# Usage

1. Open `BLRevive.exe`
2. Choose your NetMode



## NetModes

#### Local

Open the client and launch an offline botgame.

#### Online

Open the client a connect to a [MasterServer]().

#### Server

Run the [MasterServer]().



# Contribution

We're always happy about new worker bees!

Just request access to the repo and read the sources below.



**Read before contributing**:

- [contribution guidelines](https://gitlab.com/blrevive/blrevive/-/wikis/code-contribution)



## Legal/Copyright Notes

This whole project is GPL-licensed so you are free to use, share and fork it. Only commercial use is forbidden.
